-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2018 at 08:39 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cipokers`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `idBahan` int(11) NOT NULL,
  `namaBahan` varchar(50) NOT NULL,
  `kategori` int(1) NOT NULL,
  `foto` varchar(112) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`idBahan`, `namaBahan`, `kategori`, `foto`) VALUES
(7, 'Cilok', 1, 'gallery/bahan/22580624958_f52d51662c_b.jpg'),
(8, 'Macaroni', 1, 'gallery/bahan/22580624958_f52d51662c_b1.jpg'),
(9, 'Siomai', 1, 'gallery/bahan/67825284-world-stroke-day1.jpg'),
(10, 'Saos Monalisa', 2, 'gallery/bahan/67825284-world-stroke-day2.jpg'),
(11, 'Saos Duyung', 2, 'gallery/bahan/67825284-world-stroke-day3.jpg'),
(12, 'Saos Cheezy', 2, 'gallery/bahan/67825284-world-stroke-day4.jpg'),
(13, 'Sambal', 1, 'gallery/bahan/67825284-world-stroke-day5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `detailbahan`
--

CREATE TABLE `detailbahan` (
  `idDetailBahan` int(11) NOT NULL,
  `idOutlet` int(11) NOT NULL,
  `idBahan` int(11) NOT NULL,
  `stockAwal` int(11) NOT NULL,
  `stockTerpakai` int(11) NOT NULL,
  `stockSisa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detailbahan`
--

INSERT INTO `detailbahan` (`idDetailBahan`, `idOutlet`, `idBahan`, `stockAwal`, `stockTerpakai`, `stockSisa`) VALUES
(1, 1, 7, 4000, 1100, 1000),
(2, 1, 13, 1000, 24, 1000),
(3, 1, 10, 1000, 420, 1000),
(4, 1, 11, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detailtransaksi`
--

CREATE TABLE `detailtransaksi` (
  `idDetail` int(11) NOT NULL,
  `idTransaksi` int(11) NOT NULL,
  `totalPembayaran` int(11) NOT NULL,
  `totalHarga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detailtransaksi`
--

INSERT INTO `detailtransaksi` (`idDetail`, `idTransaksi`, `totalPembayaran`, `totalHarga`) VALUES
(23, 75, 50000, 30000),
(24, 76, 10000, 10000),
(25, 77, 15000, 12000),
(26, 78, 25000, 24000),
(27, 79, 50000, 36000),
(28, 80, 24000, 24000);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `idKaryawan` int(11) NOT NULL,
  `namaKaryawan` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `jenisKelamin` varchar(1) NOT NULL,
  `tanggalLahir` date NOT NULL,
  `bagian` varchar(50) NOT NULL,
  `noTelpon` varchar(12) NOT NULL,
  `foto` varchar(112) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`idKaryawan`, `namaKaryawan`, `alamat`, `jenisKelamin`, `tanggalLahir`, `bagian`, `noTelpon`, `foto`) VALUES
(3, 'Assasas', 'asasasasu', 'P', '0000-00-00', '-', '0822374323', 'gallery/karyawan/original.jpg'),
(5, 'Assasas', 'asasasasu', 'P', '0000-00-00', '-', '0822374323', 'gallery/karyawan/original.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(12) NOT NULL,
  `namalevel` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Admin'),
(2, 'Karyawan/mitra');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(11) NOT NULL,
  `namaMenu` varchar(100) NOT NULL,
  `foto` varchar(900) NOT NULL,
  `harga` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idMenu`, `namaMenu`, `foto`, `harga`) VALUES
(3, 'Cipoki Original', 'gallery/menu/original1.jpg', 12000),
(4, 'Cipoki Great', 'gallery/menu/great.jpg', 10000),
(5, 'Cipoki Twist', 'gallery/menu/twist.jpg', 10000),
(6, 'Cipoki Macs', 'gallery/menu/Macs.jpg', 10000),
(7, 'Cipoki Soms', 'gallery/menu/soms.jpg', 10000),
(8, 'Cipoki Macapocs', 'gallery/menu/Macapok.jpg', 10000),
(9, 'Cipoki Somiun', 'gallery/menu/somiun.jpg', 10000),
(10, 'Cipoki Somreng', 'gallery/menu/somreng.jpg', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE `outlet` (
  `idOutlet` int(11) NOT NULL,
  `namaOutlet` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`idOutlet`, `namaOutlet`, `alamat`) VALUES
(1, 'qwerty', 'qwqwqwqwqwq'),
(2, 'Patrang', 'jl.Soebandi no 5');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `idPesanan` int(11) NOT NULL,
  `idMenu` int(11) NOT NULL,
  `idTransaksi` int(11) NOT NULL,
  `saos` int(11) NOT NULL,
  `lvSambal` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`idPesanan`, `idMenu`, `idTransaksi`, `saos`, `lvSambal`, `harga`) VALUES
(99, 3, 77, 1, 4, 12000),
(100, 3, 78, 1, 8, 12000),
(101, 3, 78, 1, 11, 12000),
(102, 3, 79, 1, 4, 12000),
(103, 3, 79, 1, 4, 12000),
(104, 3, 79, 1, 4, 12000),
(105, 3, 80, 3, 7, 12000),
(106, 3, 80, 3, 11, 12000);

-- --------------------------------------------------------

--
-- Table structure for table `racikan`
--

CREATE TABLE `racikan` (
  `idRacikan` int(11) NOT NULL,
  `idSatuan` int(11) NOT NULL,
  `idMenu` int(11) NOT NULL,
  `idBahan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `racikan`
--

INSERT INTO `racikan` (`idRacikan`, `idSatuan`, `idMenu`, `idBahan`, `jumlah`) VALUES
(1, 1, 3, 7, 130),
(2, 1, 4, 7, 80),
(3, 2, 4, 9, 2),
(4, 1, 5, 7, 60),
(7, 1, 5, 8, 80),
(8, 2, 5, 9, 2),
(9, 1, 6, 8, 120),
(10, 1, 6, 7, 30),
(11, 1, 7, 8, 120),
(12, 2, 7, 9, 1),
(13, 1, 8, 8, 120),
(14, 2, 9, 9, 4),
(15, 2, 10, 9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `idSatuan` int(11) NOT NULL,
  `namaSatuan` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`idSatuan`, `namaSatuan`) VALUES
(1, 'Gram'),
(2, 'Biji');

-- --------------------------------------------------------

--
-- Table structure for table `toping`
--

CREATE TABLE `toping` (
  `idToping` int(11) NOT NULL,
  `nmToping` varchar(225) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `idSatuan` int(11) NOT NULL,
  `idBahan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toping`
--

INSERT INTO `toping` (`idToping`, `nmToping`, `jumlah`, `idSatuan`, `idBahan`) VALUES
(1, 'Saos monaliza', 60, 1, 10),
(2, 'Saos Duyung', 60, 1, 11),
(3, 'Saos Cheezy', 30, 1, 12),
(4, 'lv 0', 0, 1, 13),
(5, 'lv 1', 12, 1, 13),
(6, 'lv 2', 12, 1, 13),
(7, 'lv 3', 12, 1, 13),
(8, 'lv 4', 12, 1, 13),
(9, 'Saos Monaliza Spesial', 40, 1, 10),
(10, 'Saos Duyung Spesial', 40, 1, 11),
(11, 'lv 5', 0, 1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idTransaksi` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nmPembeli` varchar(225) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idTransaksi`, `idUser`, `nmPembeli`, `tanggal`, `waktu`) VALUES
(75, 143252, 'asasa', '2018-11-30', '00:00:00'),
(76, 143252, 'rotian', '2018-12-01', '00:00:00'),
(77, 143252, 'Asd', '2018-12-02', '00:00:00'),
(78, 143252, 'Hafiz', '2018-12-03', '00:00:00'),
(79, 143252, 'yogi', '2018-12-03', '00:00:00'),
(80, 143252, 'yogi', '2018-12-03', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idKaryawan` int(11) NOT NULL,
  `idOutlet` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(900) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `idKaryawan`, `idOutlet`, `username`, `password`, `level`) VALUES
(143251, 3, 1, 'sa', '$2y$10$FLhPPq2yQmkYp1mSx22lE.ywoZ6m/qo8F727zjyy8T3w.4hgwRl3W', 1),
(143252, 5, 1, 'qw', '$2y$10$ulKhC1z0JGNiJ/s3l8C4LOyhj0Z5.Oa9lAw6CHa6ktzAUkMriOcxW', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`idBahan`);

--
-- Indexes for table `detailbahan`
--
ALTER TABLE `detailbahan`
  ADD PRIMARY KEY (`idDetailBahan`),
  ADD KEY `idOutlet` (`idOutlet`),
  ADD KEY `idRacikan` (`idBahan`);

--
-- Indexes for table `detailtransaksi`
--
ALTER TABLE `detailtransaksi`
  ADD PRIMARY KEY (`idDetail`),
  ADD KEY `idTransaksi` (`idTransaksi`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`idKaryawan`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
  ADD PRIMARY KEY (`idOutlet`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`idPesanan`),
  ADD KEY `idMenu` (`idMenu`),
  ADD KEY `pesanan_ibfk_2` (`idTransaksi`),
  ADD KEY `lvSambal` (`lvSambal`),
  ADD KEY `saos` (`saos`);

--
-- Indexes for table `racikan`
--
ALTER TABLE `racikan`
  ADD PRIMARY KEY (`idRacikan`),
  ADD KEY `idMenu` (`idMenu`),
  ADD KEY `idBahan` (`idBahan`),
  ADD KEY `idSatuan` (`idSatuan`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`idSatuan`);

--
-- Indexes for table `toping`
--
ALTER TABLE `toping`
  ADD PRIMARY KEY (`idToping`),
  ADD KEY `idSatuan` (`idSatuan`),
  ADD KEY `idBahan` (`idBahan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idTransaksi`),
  ADD KEY `idPemesan` (`nmPembeli`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `user_ibfk_1` (`idOutlet`),
  ADD KEY `idKaryawan` (`idKaryawan`),
  ADD KEY `level` (`level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `idBahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `detailbahan`
--
ALTER TABLE `detailbahan`
  MODIFY `idDetailBahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `detailtransaksi`
--
ALTER TABLE `detailtransaksi`
  MODIFY `idDetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `idKaryawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
  MODIFY `idOutlet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `idPesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `racikan`
--
ALTER TABLE `racikan`
  MODIFY `idRacikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `idSatuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `toping`
--
ALTER TABLE `toping`
  MODIFY `idToping` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idTransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143253;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detailbahan`
--
ALTER TABLE `detailbahan`
  ADD CONSTRAINT `detailbahan_ibfk_2` FOREIGN KEY (`idOutlet`) REFERENCES `outlet` (`idOutlet`),
  ADD CONSTRAINT `detailbahan_ibfk_3` FOREIGN KEY (`idBahan`) REFERENCES `bahan` (`idBahan`);

--
-- Constraints for table `detailtransaksi`
--
ALTER TABLE `detailtransaksi`
  ADD CONSTRAINT `detailtransaksi_ibfk_3` FOREIGN KEY (`idTransaksi`) REFERENCES `transaksi` (`idTransaksi`);

--
-- Constraints for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD CONSTRAINT `pesanan_ibfk_1` FOREIGN KEY (`idMenu`) REFERENCES `menu` (`idMenu`),
  ADD CONSTRAINT `pesanan_ibfk_2` FOREIGN KEY (`idTransaksi`) REFERENCES `transaksi` (`idTransaksi`),
  ADD CONSTRAINT `pesanan_ibfk_3` FOREIGN KEY (`lvSambal`) REFERENCES `toping` (`idToping`),
  ADD CONSTRAINT `pesanan_ibfk_4` FOREIGN KEY (`saos`) REFERENCES `toping` (`idToping`);

--
-- Constraints for table `racikan`
--
ALTER TABLE `racikan`
  ADD CONSTRAINT `racikan_ibfk_1` FOREIGN KEY (`idMenu`) REFERENCES `menu` (`idMenu`),
  ADD CONSTRAINT `racikan_ibfk_2` FOREIGN KEY (`idBahan`) REFERENCES `bahan` (`idBahan`),
  ADD CONSTRAINT `racikan_ibfk_3` FOREIGN KEY (`idSatuan`) REFERENCES `satuan` (`idSatuan`);

--
-- Constraints for table `toping`
--
ALTER TABLE `toping`
  ADD CONSTRAINT `toping_ibfk_1` FOREIGN KEY (`idSatuan`) REFERENCES `satuan` (`idSatuan`),
  ADD CONSTRAINT `toping_ibfk_2` FOREIGN KEY (`idBahan`) REFERENCES `bahan` (`idBahan`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idOutlet`) REFERENCES `outlet` (`idOutlet`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`idKaryawan`) REFERENCES `karyawan` (`idKaryawan`),
  ADD CONSTRAINT `user_ibfk_3` FOREIGN KEY (`level`) REFERENCES `level` (`idlevel`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
