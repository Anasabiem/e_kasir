<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
		$this->load->model('M_laporan');

	}

	public function index()
	{		
		
		$data['outlet']=$this->db->get('outlet');
		$data['laporanAdmin']=$this->M_laporan->laporanAdmin();
		$this->load->view('admin/v_laporan',$data);

	}
	function getOutlet(){
	$outlet = $this->input->post('outlet');
	$tglAwal = $this->input->post('tglAwal');
	$tglAhir = $this->input->post('tglAhir');
		// $tglAwal = '2018-11-30';
		// $tglAhir = '2018-11-30';
		// $outlet = 1;
	// $data = $this->M_laporan->getLaporan($tglAwal,$tglAhir,$outlet);
	// die(var_dump($data));
	echo json_encode($tglAwal);
}
}