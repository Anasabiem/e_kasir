<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
function __construct()
    {
        parent::__construct();
        $this->load->model('M_alamin');
        $this->load->model('core');
    }
	public function index()
	{		
		$data['karyawan']=$this->M_alamin->select('karyawan');
		$data['out']=$this->M_alamin->select('outlet');
		$data['level']=$this->M_alamin->select('level');
		$data['user']=$this->M_alamin->data_admin();
		$this->load->view('admin/v_user',$data);
	}
	function t_user(){
		$nama_user = $this->input->post('nm_user');
		$nama_outlet = $this->input->post('nm_outlet');
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        $pas = md5($this->input->post('password'));
		$hashed_password = password_hash($pass, PASSWORD_DEFAULT);
        $level = $this->input->post('level');
        // die($pas);
        $data = array('idKaryawan' =>$nama_user ,
        "idOutlet" => $nama_outlet,
        "username"=>$username,
        "password"=>$hashed_password,
    	"level"=>$level);
    	// var_dump($data);
        $insert = $this->db->insert('user',$data);
        if ($data > 0) {
             $this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di Input"));
            redirect(base_url().'admin/User');  
        }else{
             $this->session->set_flashdata("Pesan",$this->core->alert_time("Data Gagal di Hapus"));
            redirect(base_url().'admin/User');
        }
	}
	function hps_bahan($id){
		$where = array('iduser'=>$id);
    $hapus = $this->M_alamin-> delete($where,'User');
    if($hapus >= 0){
      $this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
      header('location:'.base_url('admin/User')); 
    }else{
      $this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
      header('location:'.base_url('admin/User'));
      
    }
	}

    function logout(){

       $this->load->view('v_login_adm');

    }
}