<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
		$this->load->model('M_bahan');
	}
	public function index()
	{		
		$data['tyu']=$this->M_alamin->select('bahan');
		$this->load->view('admin/v_tambahbahan',$data);
	}
	public function detail(){
		$data['bahan']=$this->M_alamin->select('bahan');
		$data['outlet']=$this->M_alamin->select('outlet');
		// $where = $this->input->get('nm_outlet');
		$data['bahanOut']=$this->M_bahan->allbahan();
		$this->load->view('admin/v_viewbahan',$data);
	}

	public function t_bahan(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './gallery/bahan/',
				'allowed_types' => 'gif|jpg|png|jpeg'
			);
			$this -> load -> library ('upload',$config);
			if ($this->upload->do_upload('gambar')){
				$upload_data = $this -> upload -> data ();
				$nm_bahan = $this -> input -> post ('nm_bahan');
				$kategori = $this ->input -> post ('kategori');
				$foto = "gallery/bahan/".$upload_data['file_name'];
				$data = array(
					'namaBahan' => $nm_bahan,
					'kategori' => $kategori,
					'foto' => $foto);
				$insert_data = $this->db->insert('bahan',$data);
			}
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di simpan"));
				redirect(base_url().'admin/Bahan?$insert_data');
			} else{
				$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di simpan"));
				redirect(base_url().'admin/Bahan');
			}
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal upload"));
			redirect(base_url().'admin/Bahan');
		}
	}
	function hps_bahan($id){
		$where = array('idBahan'=>$id);
		$hapus = $this->M_alamin-> delete($where,'bahan');
		if($hapus >= 0){
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
			header('location:'.base_url('admin/Bahan')); 
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
			header('location:'.base_url('admin/bahan'));
			
		}
	}
	function plusBahan(){
		$id= $this -> input -> post ('id');
		$where['idDetailBahan']=$id;
		$s_awal = $this -> input -> post ('stokawal');
		$s_tambah = $this ->input -> post ('stoktambah');
		$hasil = $this -> input -> post ('stokawal') + $this ->input -> post ('stoktambah');
		// die($hasil);
		$data = array('stockAwal' =>$hasil  );
		$simpan = $this->db->update('detailbahan',$data,$where,$where);
		if ($simpan > 0) {
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Stok Berhasil di Tambah"));
			redirect(base_url('Admin/Bahan/detail'));
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("Stok Gagal di Tambah"));
			redirect(base_url('Admin/Bahan/detail'));
		}
		
	}
	function hps_detailbahan($id){
		$where = array('idDetailBahan'=>$id);
		$hapus = $this->M_alamin-> delete($where,'detailbahan');
		if($hapus >= 0){
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
			header('location:'.base_url('admin/Bahan/detail')); 
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
			header('location:'.base_url('admin/bahan/detail'));
			
		}
	}
	function addBahan(){
		$nmBahan = $this -> input -> post ('nmBahan');
		$stok= $this -> input -> post ('stokawal');
		$outlet= $this -> input -> post ('outlet');
		$data  = array('idOutlet' =>$outlet ,
		'idBahan'=>$bahan,
		'stockAwal'=>$stok );
		$proses = $this->db->insert('detailbahan',$data);
		if ($proses >= 0) {
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di Tambah"));
			redirect(base_url('Admin/Bahan/detail'));
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("Stok Berhasil di Tambah"));
			redirect(base_url('Admin/Bahan/detail'));
		}
	}

}