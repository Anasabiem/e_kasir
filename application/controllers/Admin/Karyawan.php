<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_alamin');
        $this->load->model('core');
    }
	public function index()
	{	
		$data['karya']=$this->M_alamin->select('karyawan');	
		$this->load->view('admin/v_karyawan',$data);

	}
	public function t_karyawan(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './gallery/karyawan/',
				'allowed_types' => 'gif|jpg|png|jpeg'
			);
			$this -> load -> library ('upload',$config);
			if ($this->upload->do_upload('gambar')){
				$upload_data = $this -> upload -> data ();
				$nama_k = $this -> input -> post ('nm_karyawan');
				$jk = $this -> input -> post ('jk_kar');
				$ttl = $this -> input -> post ('ttl_k');
				$alamat_k = $this -> input -> post ('alamat_k');
				$telp = $this -> input -> post ('no_telp');
				$foto = "gallery/karyawan/".$upload_data['file_name'];
				$data = array(
					'namaKaryawan' => $nama_k,
					'jenisKelamin' => $jk,
					'tanggalLahir' => $ttl,
					'bagian' => "-",
					'alamat' => $alamat_k,
					'noTelpon' => $telp,
					'foto' => $foto);
				$insert_data = $this->db->insert('karyawan',$data);
			}
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di simpan"));
				redirect(base_url().'admin/Karyawan');
			} else{
				$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di simpan"));
				redirect(base_url().'admin/Karyawan');
			}
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal upload"));
			redirect(base_url().'admin/Karyawan');
		}
	}
	function hps_karyawan($id){
		$where = array('idKaryawan'=>$id);
    $hapus = $this->M_alamin-> delete($where,'karyawan');
    if($hapus >= 0){
      $this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
      header('location:'.base_url('admin/Karyawan')); 
    }else{
      header('location:'.base_url('admin/Karyawan'));
      $this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
    }
	}
	public function e_karyawan(){
			$config = array('upload_path' => './gallery/karyawan/',
				'allowed_types' => 'gif|jpg|png|jpeg');
			$this -> load -> library ('upload',$config);
			$this->upload->do_upload('gambar');
				$upload_data = $this -> upload -> data ();
				$nama_k = $this -> input -> post ('nm_karyawan');
				$jk = $this -> input -> post ('jk_kar');
				$ttl = $this -> input -> post ('ttl_k');
				$alamat_k = $this -> input -> post ('alamat_k');
				$telp = $this -> input -> post ('no_telp');
				$whre['idKaryawan'] =$this->input->post('idKar');
				$foto = "gallery/karyawan/".$upload_data['file_name'];
				if ($upload_data['file_name']==null) {
				$data = array(
					'namaKaryawan' => $nama_k,
					'jenisKelamin' => $jk,
					'tanggalLahir' => $ttl,
					'bagian' => "-",
					'alamat' => $alamat_k,
					'noTelpon' => $telp);
			}else{
				$data = array(
					'namaKaryawan' => $nama_k,
					'jenisKelamin' => $jk,
					'tanggalLahir' => $ttl,
					'bagian' => "-",
					'alamat' => $alamat_k,
					'noTelpon' => $telp,
					'foto' => $foto);
			}
				$insert_data = $this->db->update('karyawan',$data,$where);
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di Rubah"));
				redirect(base_url().'admin/Karyawan');
			} else{
				$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di Rubah"));
				redirect(base_url().'admin/Karyawan');
			}
	}

	
}