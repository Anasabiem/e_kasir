<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {
function __construct()
    {
        parent::__construct();
        $this->load->model('M_alamin');
        $this->load->model('core');
    }
    public function index()
    {       
        $data['outlet']=$this->M_alamin->select('outlet');
        $this->load->view('admin/v_tambahoutlet',$data);
    }

    public function detail()
    {       
        $this->load->view('admin/v_detailoutlet');
    }

    public function tmbh_outlet(){
        $nama_outlet = $this->input->post('nm_outlet');
        $alamat_outlet = $this->input->post('alamat');
        $data = array('namaOutlet' =>$nama_outlet ,
        "alamat" => $alamat_outlet );
        $insert = $this->db->insert('outlet',$data);
        if ($data > 0) {
             $this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di Input"));
            redirect(base_url().'admin/Outlet');  
        }else{
             $this->session->set_flashdata("Pesan",$this->core->alert_time("Data Gagal di Hapus"));
            redirect(base_url().'admin/Outlet');
        }
    }
    function hps_outlet($id){
        $where = array('idOutlet'=>$id);
    $hapus = $this->M_alamin-> delete($where,'outlet');
    if($hapus >= 0){
      $this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
      header('location:'.base_url('admin/Outlet')); 
    }else{
      header('location:'.base_url('admin/Outlet'));
      $this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
    }
    }



}