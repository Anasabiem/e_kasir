<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
		$this->load->model('M_menu');
	}
	public function index()
	{		
		$data['menu']=$this->M_alamin->select('menu');
		$this->load->view('admin/v_tambahmenu',$data);
	}


	public function detail_m()
	{	
		$data['racikan']=$this->M_menu->select_racikan();	
		$data['menu']=$this->M_alamin->select('menu');
		$data['bahan']=$this->M_alamin->select('bahan');
		$data['satuan']=$this->M_alamin->select('satuan');
		$this->load->view('admin/v_racikan',$data);
	}

	function t_menu(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './gallery/menu/',
				'allowed_types' => 'gif|jpg|png|jpeg'
			);
			$this -> load -> library ('upload',$config);
			if ($this->upload->do_upload('gambar')){
				$upload_data = $this -> upload -> data ();
				$nm_menu = $this -> input -> post ('nm_menu');
				$harga = $this->input->post('harga');
				$foto = "gallery/menu/".$upload_data['file_name'];
				$data = array(
					'namaMenu' => $nm_menu,
					'foto' => $foto,
					'harga' => $harga);
				$insert_data = $this->db->insert('menu',$data);
			}
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di simpan"));
				redirect(base_url().'admin/Menu');
			} else{
				$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di simpan"));
				redirect(base_url().'admin/Menu');
			}
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal upload"));
			redirect(base_url().'admin/Menu');
		}
	}
	function e_menu(){
		$config = array('upload_path' => './gallery/menu/',
			'allowed_types' => 'gif|jpg|png|jpeg');
			
			$this -> load -> library ('upload',$config);
			$this->upload->do_upload('gambar');
			$upload_data = $this -> upload -> data ();
			$idKaryawan = $this -> input -> post ('id_menu'); 
			$nm_menu = $this -> input -> post ('nm_menu');
    		$where['idMenu']= $idKaryawan;
			$harga = $this->input->post('harga');
			$foto = "gallery/menu/".$upload_data['file_name'];
			if ($upload_data['file_name']==null) {
				$data = array(
					'namaMenu' => $nm_menu,
					'harga' => $harga);
			}else{
				$data = array(
					'namaMenu' => $nm_menu,
					'foto' => $foto,
					'harga' => $harga);
			}
			$insert_data = $this->db->update('menu',$data,$where);
		if ($insert_data >= 0) {
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di simpan"));
			redirect(base_url().'admin/Menu');
		} else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di simpan"));
			redirect(base_url().'admin/Menu');
		}
	}
	function t_racikan(){
		$menu = $this->input->post('nm_menu');
		$bahan = $this->input->post('nm_bahan');
		$jumlah = $this->input->post('jumlah');
		$satuan = $this->input->post('nm_sat');
		$data = array('idMenu' =>$menu ,
			'idBahan'=> $bahan,
			'jumlah'=> $jumlah,
			'idSatuan'=>$satuan );
		$insert_data = $this->db->insert('racikan',$data);
		if ($insert_data >= 0) {
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di upload"));
			redirect(base_url().'admin/Menu/detail_m');
		} else {
			$this->session->set_flashdata("Pesan",$this->core->alert_time("Data gagal di upload"));
			redirect(base_url().'admin/Menu/detail_m');
		}
	}
	function hps_racikan($id){
		$where = array('idRacikan'=>$id);
		$hapus = $this->M_alamin-> delete($where,'racikan');
		if($hapus >= 0){
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
			header('location:'.base_url('admin/Menu/detail_m')); 
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
			header('location:'.base_url('admin/Menu/detail_m'));
		}
	}
	function hps_menu($id){
		$where = array('idMenu'=>$id);
		$hapus = $this->M_alamin-> delete($where,'menu');
		if($hapus >= 0){
			$this->session->set_flashdata("Pesan",$this->core->alert_succes("Berhasil di Hapus"));
			header('location:'.base_url('admin/Menu')); 
		}else{
			$this->session->set_flashdata("Pesan",$this->core->alert_time("gagal Hapus"));
			header('location:'.base_url('admin/Menu'));
		}	
	}
}