<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
		$this->load->model('M_laporan');
	}
	public function index()
	{		
		$id = $this->input->post('id_transaksi');
		// die($id);
		$outlet = $this->session->userdata('idOutlet');
		$data['total'] = $this->M_laporan->jumlahPesanan($outlet);
		$data['laporan']=$this->M_laporan->laporan($outlet);
		$this->load->view('v_laporan',$data);
	}
}