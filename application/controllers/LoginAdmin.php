<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginAdmin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
	}
	public function index()
	{		
		$this->load->view('v_login_adm');
	}
	function aksi_login(){
		$username = $this->input->post('usr');
		$password = $this->input->post('psw');
		$where = array(
			'username' => $username,
		);
		
		$cek = $this->M_alamin->cek_login("user",$where)->num_rows();
		$cek1 = $this->M_alamin->cek_login("user",$where)->row();
		if($cek > 0){
			$hashed_password = $cek1->password;
			if(password_verify($password, $hashed_password)) {
				$data_session = array(
					'id_user' => $cek1->iduser,
					'status' => "login",
					'level' => $cek1->level,
					'idOutlet' => $cek1->idOutlet

				);
				echo "berhasil";
				$this->session->set_userdata($data_session);
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Login sukses"));

				if ($cek1 -> level == '1') {
					redirect(base_url("Admin/Menu?"));

				}else if ($cek1 -> level == '2') {
					redirect (base_url("Bayar?"));
				}
			} else{
				echo "Gagal";
				$this->session->set_flashdata("Pesan",$this->core->alert_time("Password Belum terdaftar"));
			}
			
		}else{
			echo "berhasil";
			$this->session->set_flashdata("Pesan",$this->core->alert_time("Username & Password tidak terdaftar"));
			redirect(base_url("Loginadmin?"));
		}
	}
	function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata("Pesan",$this->core->alert_succes("Login sukses"));
		redirect(base_url('LoginAdmin'));
	}
}