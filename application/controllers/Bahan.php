<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahan extends CI_Controller {
function __construct()
    {
        parent::__construct();
        $this->load->model('M_alamin');
        $this->load->model('core');
        $this->load->model('M_bahan');
    }
	public function index()
	{		
		$where = $this->session->userdata('idOutlet');
		$data['tyu']=$this->M_bahan->bahanOutlet($where);
		$this->load->view('v_bahan',$data);
	}
}