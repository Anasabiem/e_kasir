<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bayar extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_alamin');
		$this->load->model('core');
		$this->load->model('M_menu');
	}
	public function index()
	{
		$data['sambal']=$this->M_menu->selectsambal();
		$data['saos'] = $this->M_menu->selectsaos();
		$data['bayar'] = $this->M_alamin->select ('menu');
		$this->load->view('v_transaksi',$data);	
	}
	public function t_transaksi(){
		$pembeli = $this->input->post('nmPembeli');
		$menu = $this->input->post('id_menu');
		$saos = $this->input->post('saos');
		$sambal = $this->input->post('sambal');
		$harga = $this->input->post('harga');
		$user = $this->session->userdata('id_user');
		$tangga = date("Y-m-d");
		$jam = time("h:i:s");
		// die($saos);
		$data = array(
			'idUser' => $user,
			'nmPembeli' => $pembeli,
			'tanggal'=> $tangga,
			'waktu'=>$jam);
		$insert = $this->M_alamin->insert2('transaksi',$data);
			for ($i=0; $i < count($menu); $i++) { 
				$data = array(
				'idMenu' => $menu[$i],
				'idTransaksi' => $insert,
				'saos'=> $saos[$i],
				'lvSambal'=>$sambal[$i],
				'harga'=> $harga[$i]);
				$kirim = $this->M_alamin->insert('pesanan',$data);
				$getRacikan = $this->M_menu->getRacikan($menu[$i])->result();
				foreach ($getRacikan as $value) {
					$jumlahBahan = $value->jumlah;
					$getBahan = $this->db->get_where('detailbahan',array('idBahan'=>$value->idBahan))->row_array();
					$stockTerpakai = $getBahan['stockTerpakai']+$jumlahBahan;
					$this->db->where('idBahan',$value->idBahan);
					$this->db->update('detailbahan',array('stockTerpakai'=>$stockTerpakai ));
					$this->db->reset_query();
				}
				$toping = $this->M_menu->getTopping($saos[$i]);
				$jumlahBahan = $toping["jumlah"];
				$detilBahan = $this->M_menu->get_detilBahan($toping["idBahan"]);
				$this->db->where('idBahan',$toping["idBahan"]);
				$this->db->update('detailbahan',array('stockTerpakai'=>$detilBahan['stockTerpakai']+$jumlahBahan ));
				$toping1 = $this->M_menu->getTopping($sambal[$i]);
				$jumlahBahan1 = $toping1["jumlah"];
				$detilBahan1 = $this->M_menu->get_detilBahan($toping1["idBahan"]);
				$this->db->where('idBahan',$toping1["idBahan"]);
				$ret = $this->db->update('detailbahan',array('stockTerpakai'=>$detilBahan1['stockTerpakai']+$jumlahBahan1 ));
				// die(var_dump($ret));
			}
			$total = $this->input->post('total');
			$bayar = $this->input->post('pembayaran');
			$data = array('idTransaksi'=>$insert,
				'totalPembayaran'=> $bayar, 'totalHarga'=>$total
			);
			$kirimkan = $this->M_alamin->insert('detailtransaksi',$data);
			if ($kirimkan > 0) {
				$this->session->set_flashdata("Pesan",$this->core->alert_succes("Data Berhasil di simpan"));
				redirect(base_url().'Bayar');
			}

			redirect(base_url("Bayar"));
	}


}