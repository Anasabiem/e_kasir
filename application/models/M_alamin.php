<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_alamin extends CI_Model {

    public function select($table){
    return $this->db->get($table);
  }
  public function selectlimit($table){
      $this->db->order_by('id_informasi','DESC') ;
     $this->db->limit(3) ;
     return $this->db->get($table) ;
  }
  public function selectwhere($table,$data){
    return $this->db->get_where($table, $data);
  }

  function delete($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  public function update($table,$data,$where){
    $this->db->update($table,$data,$where);
  }

  public function insert($table,$data){
    $this->db->insert($table,$data);
  }
  public function insert2($table,$data){
    $this->db->insert($table,$data);
    return $this->db->insert_id();
  }
  function cek_login($table,$where){
    return $this->db->get_where($table,$where);
  }
  function data_admin(){
    $this->db->select('user.*, karyawan.*, outlet.*');
          $this->db->join('karyawan', 'user.idKaryawan = karyawan.idKaryawan');
          $this->db->join('outlet', 'user.idOutlet = outlet.idOutlet');
          $this->db->from('user');
          // $this->db->where('admin.id_admin', $id);
          $data=$this->db->get();
          return $data;
  } 
  function select_multy(){

  }
}
  
  
