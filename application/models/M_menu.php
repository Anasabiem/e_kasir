<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menu extends CI_Model {
  function select_racikan(){
    $this->db->select('racikan.*, satuan.*, menu.*, bahan.*');
          $this->db->join('satuan', 'satuan.idSatuan = racikan.idSatuan');
          $this->db->join('menu', 'menu.idMenu= racikan.idMenu');
          $this->db->join('bahan', 'bahan.idBahan= racikan.idBahan');
          $this->db->from('racikan');
          // $this->db->where('harga.id_harga', '1');
          $data=$this->db->get();
          return $data;
  }
 public function selectsaos(){
    return $this->db->query("SELECT * from toping where nmToping like '%saos%'");
    // return $this->db->get_where($table, $data);
  }
  public function selectsambal(){
    return $this->db->query("SELECT * from toping where nmToping like '%lv%'");
    // return $this->db->get_where($table, $data);
  }
  public function getRacikan($idMenu){
    $this->db->select('racikan.*, menu.*');
          $this->db->join('menu', 'menu.idMenu= racikan.idMenu');
          $this->db->from('racikan');
          $this->db->where("menu.idMenu",$idMenu);
          $data=$this->db->get();
          return $data;
  }
  public function getTopping($idToping){
    return $this->db->get_where("toping",array("idToping"=>$idToping))->row_array();
  }
  public function get_detilBahan($id_bahan){
    return $this->db->get_where("detailbahan",array("idBahan"=>$id_bahan))->row_array();
  }

}
