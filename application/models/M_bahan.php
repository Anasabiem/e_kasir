<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Bahan extends CI_Model{
	
	function allbahan(){
		$this->db->select('detailbahan.*, bahan.*');
		$this->db->join('bahan', 'bahan.idBahan = detailbahan.idBahan' );
		// $this->db->join('detailtransaksi', 'detailtransaksi.idTransaksi = transaksi.idTransaksi');
		// $this->db->join('pesanan', 'pesanan.idTransaksi = transaksi.idTransaksi');
		// $this->db->join('menu', 'menu.idMenu = pesanan.idMenu');
		// $this->db->where('detailbahan.idOutlet',$id);
		$this->db->from('detailbahan');
		$data = $this->db->get();
		return $data;

	}

	function bahanOutlet($id){
		$this->db->select('detailbahan.*, bahan.*');
		$this->db->join('bahan', 'bahan.idBahan = detailbahan.idBahan' );
		// $this->db->join('user', 'user.idOutlet = detailbahan.idOutlet' );
		$this->db->where('detailbahan.idOutlet',$id);
		$this->db->from('detailbahan');
		$data = $this->db->get();
		return $data;
	}

}