<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_laporan extends CI_Model{
	
	function laporan($id){
		$this->db->select('transaksi.*, user.*, detailtransaksi.*');
		$this->db->join('user', 'transaksi.idUser = user.iduser' );
		$this->db->join('detailtransaksi', 'detailtransaksi.idTransaksi = transaksi.idTransaksi');
		// $this->db->join('pesanan', 'pesanan.idTransaksi = transaksi.idTransaksi');
		// $this->db->join('menu', 'menu.idMenu = pesanan.idMenu');
		$this->db->where('user.idOutlet',$id);
		$this->db->from('transaksi');
		$data = $this->db->get();
		return $data;

	}

	function jumlahPesanan($id){
		$this->db->select('transaksi.*, user.*, detailtransaksi.*, pesanan.*');
		$this->db->join('user', 'transaksi.idUser = user.iduser' );
		$this->db->join('detailtransaksi', 'detailtransaksi.idTransaksi = transaksi.idTransaksi');
		$this->db->join('pesanan', 'pesanan.idTransaksi = transaksi.idTransaksi');
		$this->db->where('user.idOutlet',$id);
		$this->db->from('transaksi');
		$data = $this->db->count_all_results();
		return $data;

	}

	function laporanAdmin(){
		$this->db->select('transaksi.*, detailtransaksi.*');
		$this->db->join('detailtransaksi','detailtransaksi.idTransaksi = transaksi.idTransaksi');
		$this->db->from('transaksi');
		$data = $this->db->get();
		return $data;

	}

	function getLaporan($awal,$ahir,$outlet){
		// $hari= date("d");
		$this->db->select('transaksi.*, detailtransaksi.*, user.*');
		$this->db->join('detailtransaksi','detailtransaksi.idTransaksi = transaksi.idTransaksi');
		$this->db->join('user','transaksi.idUser= user.idUser');
		$this->db->where('DATE(transaksi.tanggal) >= ',$awal);
		$this->db->where('DATE(transaksi.tanggal) <= ',$ahir);
		$this->db->where('user.idOutlet',$outlet);
		$this->db->from('transaksi');
		$data = $this->db->get();
		return $data->result();
	}
}