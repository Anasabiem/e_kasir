<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php $this->load->view('side/head'); ?>
<body class="fixed-header dashboard">
  <?php $this->load->view('side/sidebarAdmin'); ?>
  <div class="page-container " style="margin-top: 120px;">
    <div class="col-md-12 crd" >
      <div class=" container-fluid   container-fixed-lg">

        <div class="card card-transparent">
          <div class="card-header ">
            <div class="card-title">Laporan Penjualan
            </div>
            
            <div class="pull-right">
              <div class="col-md-12">
                <div class="form-group">
                  <h6>Pilih Outlet</h6>
                  <select id="pilihOutlet">
                    <option>Pilih Outlet</option>
                    <?php foreach ($outlet->result() as $out): ?>
                      <option value="<?php echo $out->idOutlet; ?>"><?php echo $out->namaOutlet; ?></option>
                    <?php endforeach ?>
                  </select>                  

              </div>
            </div>
          </div>
          <div class="pull-right">
              <div class="col-md-12">
                <div class="form-group">
                  <h6>Sampai Tanggal..</h6>
                  <input type="date" name="" id="tglAhir" required="">
                  

              </div>
            </div>
          </div>
          <div class="pull-right">
              <div class="col-md-12">
                <div class="form-group">
                  <h6>Dari Tanggal.. </h6>
                  <input type="date" id="tglAwal" name="" required="">

                  </select>
                  

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="card-body">
          <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithDynamicRows">
            <thead>
              <tr>
                <th>nomor_transaksi</th>
                <th>Tanggal, Jam</th>
                <th>Menu</th>
                <th>Jumlah Transaksi</th>
              </tr>
            </thead>


            <?php foreach ($laporanAdmin->result() as $laporan){ ?>
              <tbody>
                <tr>
                  <td class="v-align-middle">
                    <p><?php echo $laporan->idTransaksi;  ?></p>
                  </td>
                  <td class="v-align-middle">
                    <p><?php echo $laporan->tanggal; ?></p>
                  </td>
                  <td class="v-align-middle">
                    <p>FREE</p>
                  </td>
                  <td class="v-align-middle">
                    <p>Rp. <?php echo $laporan->totalHarga ; ?></p>
                  </td>
                </tr>
              </tbody>
            <?php } ?>
          </table>
        </div>
      </div>

    </div>
  </div></div>
  <div class="modal fade stick-up" id="addNewAppModal" tabindex="-1" role="dialog" aria-labelledby="addNewAppModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header clearfix ">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h4 class="p-b-5"><span class="semi-bold">Tambah</span> Outlet</h4>
        </div>
        <div class="modal-body">
          <p class="small-text">Create a new app using this form, make sure you fill them all</p>
          <form role="form">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group form-group-default">
                  <label>name</label>
                  <input id="appName" type="text" class="form-control" placeholder="Name of your app">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group form-group-default">
                  <label>Description</label>
                  <input id="appDescription" type="text" class="form-control" placeholder="Tell us more about it">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group form-group-default">
                  <label>Price</label>
                  <input id="appPrice" type="text" class="form-control" placeholder="your price">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-group-default">
                  <label>Notes</label>
                  <input id="appNotes" type="text" class="form-control" placeholder="a note">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button id="add-app" type="button" class="btn btn-primary  btn-cons">Add</button>
          <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>

  </div>

  <!-- <?php $this->load->view('side/footer');?> -->
  <?php $this->load->view('side/header'); ?>
  <?php $this->load->view('side/js'); ?>
  <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
  <script type="<?php echo base_url() ?>master/adm/text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>master/adm/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
  <script src="<?php echo base_url() ?>master/adm/assets/js/datatables.js" type="text/javascript"></script>
  <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy033pPoW0sJlOoZaOdbiAu405UZoAWbzRjORxYExBu7uP1EqbOsiG5IYxVCXJbcWuCkD0Ay%2b7dXMkpc1Nd98lpMR2bbDMY7NvsUSzNXzIGNIcW153tfYJJykMbEeMApkA61IVkEzFlBkx%2f2XGHC69M%2bPxYboQw75Fb5ctbUossMoGXhesW%2fSAr2N9sfLRC4QWIjPDzSyQENRRWHwV9qaG4MlNaRSMhJqZZDk2m4O9b4PYYG1hA3NPyYIjwHyf%2f4eqxr9PutulvjZ2AzEgz2mIMG6O%2fExc9f3D5NVRTLZ8g7eCfrSq5c%2f3hip3MnYwAqiP6GgE3QnQDHlVVHgjFU3ShrqZVbver60ol83z457MsXjFiStT%2fSavAB8ZIkiL5XekmmRzlwG%2b%2bl5mHO7SHVhhK%2bjCFhSe8l8Ab%2ba1x1427VUQ08LvY3M2kR3ZJk3zeNKFCIjh%2b1zLfJTjuMAHKrSyDQZvewbeByBu8lqaj%2feTjYlEYsrXpGdgB1zO" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>


<script type="text/javascript">
  $(document).ready(function(){
    $(document).on("change","#pilihOutlet",function(){
      var outlet = $("#pilihOutlet").val();
    var tglAwal = $("#tglAwal").val();
      var tglAhir = $("#tglAhir").val();
      alert(outlet);

    });
    $("button").click(function(){
        $.post("demo_test_post.asp",
        {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data,status){
            alert("Data: " + data + "\nStatus: " + status);
        });
    });

    // var isi = ;
});
</script>


</body>
</html>
