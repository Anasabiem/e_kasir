<div class=" container-fluid  container-fixed-lg footer" style="margin-left: 50px; margin-top:200px;">
								
								<div class="copyright sm-text-center">
									<p class="small no-margin pull-left sm-pull-reset">
										<span class="hint-text">Copyright &copy; 2017 </span>
										<span class="font-montserrat">REVOX</span>.
										<span class="hint-text">All rights reserved. </span>
										<span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> <span class="muted">|</span> <a href="#" class="m-l-10">Privacy Policy</a></span>
									</p>
									<p class="small no-margin pull-right sm-pull-reset">
										Hand-crafted <span class="hint-text">&amp; made with Love</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
							<style type="text/css">
								.act-btn{
            background:green;
            display: block;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            color: white;
            font-size: 30px;
            font-weight: bold;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            text-decoration: none;
            transition: ease all 0.3s;
            position: fixed;
            right: 30px;
            bottom:30px;
        }
.act-btn:hover{background: blue}
							</style>