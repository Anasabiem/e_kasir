<nav class="page-sidebar" data-pages="sidebar">

	<div class="sidebar-overlay-slide from-top" id="appMenu">
		<div class="row">
			<div class="col-xs-6 no-padding">
				<a href="#" class="p-l-40"><img src="<?php echo base_url() ?>master/adm/assets/img/demo/social_app.svg" alt="socail">
				</a>
			</div>
			<div class="col-xs-6 no-padding">
				<a href="#" class="p-l-10"><img src="<?php echo base_url() ?>master/adm/assets/img/demo/email_app.svg" alt="socail">
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 m-t-20 no-padding">
				<a href="#" class="p-l-40"><img src="<?php echo base_url() ?>master/adm/assets/img/demo/calendar_app.svg" alt="socail">
				</a>
			</div>
			<div class="col-xs-6 m-t-20 no-padding">
				<a href="#" class="p-l-10"><img src="<?php echo base_url() ?>master/adm/assets/img/demo/add_more.svg" alt="socail">
				</a>
			</div>
		</div>
	</div>

	<div class="sidebar-header">
		<img src="<?php echo base_url() ?>master/adm/assets/img/logo_white.png" alt="logo" class="brand" data-src="<?php echo base_url() ?>master/adm/assets/img/logo_white.png" data-src-retina="<?php echo base_url() ?>master/adm/assets/img/logo_white_2x.png" width="78" height="22">
		<div class="sidebar-header-controls">
			<button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
			</button>
			<button type="button" class="btn btn-link d-lg-inline-block d-xlg-inline-block d-md-inline-block d-sm-none d-none" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
			</button>
		</div>
	</div>
	
	<br><br>
	<div class="sidebar-menu">

		<ul class="menu-items">

			<li class="m-t-30 ">
				<a href="<?php echo base_url('Bayar');?>" class="detailed">
					<span class="title">Transaksi</span>
				</a>
				<span class="<?php if($this->uri->segment('1')=="Bayar" || $this->uri->segment('1')==""){ echo "bg-danger";}?> icon-thumbnail"><i class="pg-shopping_cart"></i></span>
			</li>
			<li class="">
				<a href="<?php echo base_url('Bahan');?>" class="detailed">
					<span class="title">Bahan</span>
					<!-- <span class="details">20 Bahan</span> -->
				</a>
				<span class="<?php if($this->uri->segment('1')=="Bahan"){ echo "bg-danger";}?> icon-thumbnail"><i class="pg-grid"></i></span>
			</li>
			<li class="">
				<a href="<?php echo base_url('Laporan');?>"><span class="title">Laporan</span></a>
				<span class="<?php if($this->uri->segment('1')=="Laporan"){ echo "bg-danger";}?> icon-thumbnail"><i class="pg-printer"></i></span>
			</li>
			<div class="clearfix"></div>
		</ul>
		<div class="clearfix"></div>
	</div>

</nav>
<div class="header ">

	<a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu" data-toggle="sidebar">
	</a>

	<div class="">
		<div class="brand inline   ">
			<img src="<?php echo base_url() ?>master/adm/assets/img/logo.png" alt="logo" data-src="<?php echo base_url() ?>master/adm/assets/img/logo.png" data-src-retina="<?php echo base_url() ?>master/adm/assets/img/logo_2x.png" width="78" height="22">
		</div>

	</div>
	<div class="d-flex align-items-center">

		<div class="pull-left p-r-10 fs-14 font-heading d-lg-block d-none">
			<span class="semi-bold">David</span> <span class="text-master">Nest</span>
		</div>
		<div class="dropdown pull-right d-lg-block d-none">
			<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="thumbnail-wrapper d32 circular inline">
					<img src="<?php echo base_url() ?>master/adm/assets/img/profiles/avatar.jpg" alt="" data-src="<?php echo base_url() ?>master/adm/assets/img/profiles/avatar.jpg" data-src-retina="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
				</span>
			</button>
			<div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
				<a href="#" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
				<a href="#" class="dropdown-item"><i class="pg-outdent"></i> Feedback</a>
				<a href="#" class="dropdown-item"><i class="pg-signals"></i> Help</a>
				<a href="<?php echo base_url('Admin/User/logout') ?>" class="clearfix bg-master-lighter dropdown-item">
					<span class="pull-left">Logout</span>
					<span class="pull-right"><i class="pg-power"></i></span>
				</a>
			</div>
		</div>

		<a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block badge-notif badgeCount" data-toggle="quickview" data-toggle-element="#quickview" data-badge="0"></a>
	</div>
</div>
<style type="text/css">
	.badge-notif {
        position:relative;
}

.badge-notif[data-badge]:after {
        content:attr(data-badge);
        position:absolute;
        top:-10px;
        right:-10px;
        font-size:.7em;
        background:#e53935;
        color:white;
        width:18px;
        height:18px;
        text-align:center;
        line-height:18px;
        border-radius: 50%;
}
</style>