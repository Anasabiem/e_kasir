<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>CIPOKERS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<script src="<?php echo base_url() ?>master/adm/cdn-cgi/apps/head/QJpHOqznaMvNOv9CGoAdo_yvYKU.js"></script><link rel="apple-touch-icon" href="pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>master/adm/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>master/adm/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>master/adm/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>master/adm/favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo base_url() ?>master/adm/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url() ?>master/adm/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="<?php echo base_url() ?>master/adm/pages/css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>master/adm/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>master/adm/assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    
    <link href="<?php echo base_url() ?>master/adm/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

<link href="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">

<link href="<?php echo base_url() ?>master/adm/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
</head>