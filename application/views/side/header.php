										<div id="quickview" class="quickview-wrapper" data-pages="quickview">
											<form method="post" action="<?php echo base_url('Bayar/t_transaksi') ?>">
						<ul class="nav nav-tabs" role="tablist">
							<li class="">
								<a href="#quickview-bayar" data-target="#quickview-bayar" data-toggle="tab" role="tab">Detail</a>
							</li>
							<li class="">
								<a href="#quickview-notes" data-target="#quickview-notes" data-toggle="tab" role="tab">Notes</a>
							</li>
							<li class="">
								<a class="active" href="#quickview-chat" data-toggle="tab" role="tab">Chat</a>
							</li>
						</ul>
						<a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>

						<div class="tab-content">
							

							
							<div class="tab-pane active " id="quickview-bayar">
								<div class="view-port clearfix" id="alerts" >

									<div class="view bg-white" >

										<div class="navbar navbar-default navbar-sm">
											<div class="navbar-inner">

												<a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
													<i class="pg-more"></i>
												</a>

												<div class="view-heading">
													Notications
												</div>

												<a href="#" class="inline action p-r-10 pull-right link text-master">
													<i class="pg-search"></i>
												</a>

											</div>
										</div>


										<div data-init-list-view="ioslist" class="list-view boreded no-top-border" ">
											<div class="list-view-group-container">

												<div class="list-view-group-header text-uppercase">
													Detail Pemesan
												</div>

												<ul>
													<li class="alert-list">

														<a href="javascript:;" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
															<span>Nama : </span><input type="text" name="nmPembeli" required="">
														</a>
													</li>
												</ul>
											</div>
											<div class="list-view-group-container">
												<div class="list-view-group-header text-uppercase">
													List Pesanan
												</div>
												<ul class="pesanan" style="padding-bottom: 250px;"></ul>
											</div>
											<li>
											<div class="list-view-group-container" style="position: absolute; bottom: 100px; padding-left:  12px;">
												<div class="list-view-group-header text-uppercase">
													Pembayaran
												</div>	
											</div>
											</li>
										</div>
										<div style="bottom: 0px; position: absolute; background-color: white; width: 100%; padding-bottom: 80px; padding: 10px;">
											<hr>
												<!-- <input type="text" name="total" id="total" hidden=""> -->
												<span >Total : <span style="float: right; padding-bottom: 10px">Rp.<input  id="total" type="text" name="total" style="margin-bottom: -35px;" readonly=""></span></span><br>
												<span>Bayar :<span style="float: right;padding-bottom: 5px ">Rp.<input type="text" id="bayar" name="pembayaran" class="bayarin" placeholder=",-" required=""></span></span><br>
												<span>Kembalian : <span style="float: right; padding-bottom: 5px">Rp. <span class="kembalian"></span></span></span><br>
												<div style="float: right;">
												<a href="<?php echo base_url('Bayar') ?>" type="button"><button class="btn btn-cons">Batal</button></a>
												<button class="btn btn-primary btn-cons">Selesai</button></div>
												</div>
									</div>

								</div>
							</div>
							</form>
							<div class="tab-pane no-padding" id="quickview-notes">
								<div class="view-port clearfix quickview-notes" id="note-views">

									<div class="view list" id="quick-note-list">
										<div class="toolbar clearfix">
											<ul class="pull-right ">
												<li>
													<a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
												</li>
												<li>
													<a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
												</li>
											</ul>
											<button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
										</div>
										<ul>

											<li data-noteid="1">
												<div class="left">

													<div class="checkbox check-warning no-margin">
														<input id="qncheckbox1" type="checkbox" value="1">
														<label for="qncheckbox1"></label>
													</div>


													<p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

												</div>

												<div class="right pull-right">

													<span class="date">12/12/14</span>
													<a href="#" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-chevron-right"></i></a>

												</div>

											</li>


											<li data-noteid="2">
												<div class="left">

													<div class="checkbox check-warning no-margin">
														<input id="qncheckbox2" type="checkbox" value="1">
														<label for="qncheckbox2"></label>
													</div>


													<p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

												</div>

												<div class="right pull-right">

													<span class="date">12/12/14</span>
													<a href="#"><i class="fa fa-chevron-right"></i></a>

												</div>

											</li>


											<li data-noteid="2">
												<div class="left">

													<div class="checkbox check-warning no-margin">
														<input id="qncheckbox3" type="checkbox" value="1">
														<label for="qncheckbox3"></label>
													</div>


													<p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

												</div>

												<div class="right pull-right">

													<span class="date">12/12/14</span>
													<a href="#"><i class="fa fa-chevron-right"></i></a>

												</div>

											</li>


											<li data-noteid="3">
												<div class="left">

													<div class="checkbox check-warning no-margin">
														<input id="qncheckbox4" type="checkbox" value="1">
														<label for="qncheckbox4"></label>
													</div>


													<p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

												</div>

												<div class="right pull-right">

													<span class="date">12/12/14</span>
													<a href="#"><i class="fa fa-chevron-right"></i></a>

												</div>

											</li>


											<li data-noteid="4">
												<div class="left">

													<div class="checkbox check-warning no-margin">
														<input id="qncheckbox5" type="checkbox" value="1">
														<label for="qncheckbox5"></label>
													</div>


													<p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

												</div>

												<div class="right pull-right">

													<span class="date">12/12/14</span>
													<a href="#"><i class="fa fa-chevron-right"></i></a>

												</div>

											</li>

										</ul>
									</div>

									<div class="view note" id="quick-note">
										<div>
											<ul class="toolbar">
												<li><a href="#" class="close-note-link"><i class="pg-arrow_left"></i></a>
												</li>
												<li><a href="#" data-action="Bold" class="fs-12"><i class="fa fa-bold"></i></a>
												</li>
												<li><a href="#" data-action="Italic" class="fs-12"><i class="fa fa-italic"></i></a>
												</li>
												<li><a href="#" class="fs-12"><i class="fa fa-link"></i></a>
												</li>
											</ul>
											<div class="body">
												<div>
													<div class="top">
														<span>21st april 2014 2:13am</span>
													</div>
													<div class="content">
														<div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane  no-padding" id="quickview-chat">
								<div class="view-port clearfix" id="chat">
									<div class="view bg-white">

										<div class="navbar navbar-default">
											<div class="navbar-inner">

												<a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
													<i class="pg-plus"></i>
												</a>

												<div class="view-heading">
													Chat List
													<div class="fs-11">Show All</div>
												</div>

												<a href="#" class="inline action p-r-10 pull-right link text-master">
													<i class="pg-more"></i>
												</a>

											</div>
										</div>

										<div data-init-list-view="ioslist" class="list-view boreded no-top-border">
											<div class="list-view-group-container">
												<div class="list-view-group-header text-uppercase">
												a</div>
												<ul>

													<li class="chat-user-list clearfix">
														<a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
															<span class="thumbnail-wrapper d32 circular bg-success">
																<img width="34" height="34" alt="" data-src-retina="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/1x.jpg" data-src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/1.jpg" src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/1x.jpg" class="col-top">
															</span>
															<p class="p-l-10 ">
																<span class="text-master">ava flores</span>
																<span class="block text-master hint-text fs-12">Hello there</span>
															</p>
														</a>
													</li>

												</ul>
											</div>
										</div>
									</div>

									<div class="view chat-view bg-white clearfix">

										<div class="navbar navbar-default">
											<div class="navbar-inner">
												<a href="javascript:;" class="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
													<i class="pg-arrow_left"></i>
												</a>
												<div class="view-heading">
													John Smith
													<div class="fs-11 hint-text">Online</div>
												</div>
												<a href="#" class="link text-master inline action p-r-10 pull-right ">
													<i class="pg-more"></i>
												</a>
											</div>
										</div>


										<div class="chat-inner" id="my-conversation">

											<div class="message clearfix">
												<div class="chat-bubble from-me">
													Hello there
												</div>
											</div>


											<div class="message clearfix">
												<div class="profile-img-wrapper m-t-5 inline">
													<img class="col-top" width="30" height="30" src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small.jpg" alt="" data-src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small.jpg" data-src-retina="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small2x.jpg">
												</div>
												<div class="chat-bubble from-them">
													Hey
												</div>
											</div>


											<div class="message clearfix">
												<div class="chat-bubble from-me">
													Did you check out Pages framework ?
												</div>
											</div>


											<div class="message clearfix">
												<div class="chat-bubble from-me">
													Its an awesome chat
												</div>
											</div>


											<div class="message clearfix">
												<div class="profile-img-wrapper m-t-5 inline">
													<img class="col-top" width="30" height="30" src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small.jpg" alt="" data-src="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small.jpg" data-src-retina="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small2x.jpg">
												</div>
												<div class="chat-bubble from-them">
													Yea
												</div>
											</div>

										</div>


										<div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
											<div class="row">
												<div class="col-1 p-t-15">
													<a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
												</div>
												<div class="col-8 no-padding">
													<input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
												</div>
												<div class="col-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
													<a href="#" class="link text-master"><i class="pg-camera"></i></a>
												</div>
											</div>
										</div>

									</div>

								</div>
							</div>
						</div>

						<script type="text/javascript">
							$(document).ready(function(){
    $(document).on("click",".tampil",function(){
    	// $(".menu").toggle();
    	var id = $(this).attr("id");
    	$("#topping"+id).toggle();
    });
});
						</script>